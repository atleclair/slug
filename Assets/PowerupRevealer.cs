﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PowerupRevealer : MonoBehaviour
{
    public TextMeshProUGUI distanceText;

    public Image bw0;
    public Image bw1;
    public Image bw2;
    public Image bw3;
    public Image bw4;


    // Start is called before the first frame update
    void Start()
    {
        GameEvents.instance.OnAcornPower_Pierce += HideBW0;
        GameEvents.instance.OnAcornPower_Explode += HideBW1;
        GameEvents.instance.OnAcornPower_Shotgun += HideBW2;
        GameEvents.instance.OnAcornPower_Distance += HideBW3;
        GameEvents.instance.OnAcornPower_Bounce += HideBW4;
    }

    void Update()
    {
        distanceText.text = Mathf.RoundToInt(DistanceTraveled.Instance.distance).ToString();
    }

    void HideBW0()
    {
        bw0.enabled = false;
    }

    void HideBW1()
    {
        bw1.enabled = false;

    }
    
    void HideBW2()
    {
        bw2.enabled = false;

    }
    
    void HideBW3()
    {
        bw3.enabled = false;

    }

    void HideBW4()
    {
        bw4.enabled = false;

    }
}
