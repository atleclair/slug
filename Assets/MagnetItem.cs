﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnetItem : SpawnableObject, Poolable
{
    bool isCollected; 

    public void Ready()
    {
        isCollected = false;
    }

    public void Reset()
    {
        isCollected = false;
    }

    public override void Setup()
    {
        spawnPadding = 3;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "player" && !isCollected)
        {
            AudioManager.Instance.Play("zap");
            Magnet.Instance.Magnetize();
            StartCoroutine(Deactivate());
            isCollected = true;
        }
    }

    IEnumerator Deactivate()
    {
        GetComponent<Animator>().SetTrigger("collected");
        yield return new WaitForSeconds(.5f);
        ObjectPooler.Instance.Deactivate(gameObject);
    }
}
