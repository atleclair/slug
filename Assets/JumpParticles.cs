﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpParticles : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //GameEvents.instance.OnPlayerJump += SpawnDust;
    }

    void SpawnDust()
    {
        transform.position = Player.Instance.transform.position;
        GetComponent<Animator>().SetTrigger("jump");
    }
}
