﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EnemyProjectile : Projectile
{

    private void Start()
    {
        speed = 15f;
        damage = .5f;
        gravityAmount = 0;
        screenShakeAmount = .01f;
        activeTime = 1;
        GetComponent<Rigidbody2D>().gravityScale = gravityAmount;
        
        GetComponent<Rigidbody2D>().drag = 2;
    }

    public float DealDamage()
    {
        return damage;
    }

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Contains("player"))
        {
            Instantiate(particles, transform.position, Quaternion.identity);
            particles.Play();
        }
        Destroy(gameObject);
    }

    public override void OnCollisionEnter2D(Collision2D collision)
    {
            PlayParticles();
            Destroy(gameObject);
        
    }

}
