﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PenetrateProjectile : Projectile
{
    // Start is called before the first frame update
    void Start()
    {
        activeTime = 2;
        speed = 15f;
        screenShakeAmount = .03f;
    }

    public override void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Contains("enemy"))
        {
            AudioManager.Instance.Play("Damage");
            PlayParticles();
            ScreenShake.Instance.TriggerShake(shakeMag: screenShakeAmount);

            Enemy enemy = collision.gameObject.GetComponent<Enemy>();
            if (enemy) enemy.TakeDamage(damage);
        }
    }
}
