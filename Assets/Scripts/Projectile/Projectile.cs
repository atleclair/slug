﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class Projectile : ShootableObject
{
    protected bool isPiercing;
    protected bool canExplode;

    [SerializeField] protected float gravityAmount;

    public Explosion explosion;

    private bool explosionEnabled;

    private void Start()
    {
        speed = 10f;
        damage = 1;
        gravityAmount = 0;
        screenShakeAmount = .01f;
        activeTime = 1;
        explosionEnabled = false;
        GetComponent<Rigidbody2D>().gravityScale = gravityAmount;
        GetComponent<Rigidbody2D>().drag = 2;

        GameEvents.instance.OnOrange1 += IncreaseProjectileLife;


    }

    public override void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Contains("enemy"))
        {
            Enemy enemy = collision.gameObject.GetComponent<Enemy>();
            enemy.TakeDamage(damage);
        }

        else
        {
            PlayParticles();
            Destroy(gameObject);
        }
    }

    public virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Contains("enemy"))
        {
           //Instantiate(explosion, collision.transform.position, Quaternion.identity);

            AudioManager.Instance.Play("Damage");
            PlayParticles();
            ScreenShake.Instance.TriggerShake(shakeMag: screenShakeAmount);

            Enemy enemy = collision.gameObject.GetComponent<Enemy>();
            if (enemy) enemy.TakeDamage(damage);

            if (AcornManager.Instance.canExplode)
            {
                Instantiate(explosion, transform.position, Quaternion.identity);
            }

            if (!AcornManager.Instance.isPiercing)
            {
                Destroy(gameObject);
            }
            
        }
    }

    protected void PlayParticles()
    {
        Instantiate(particles, transform.position, Quaternion.identity);
        particles.Play();
    }

    protected virtual void IncreaseProjectileLife(string powerup)
    {
        activeTime *= 2;
    }
}
