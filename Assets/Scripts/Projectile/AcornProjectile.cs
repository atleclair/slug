﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcornProjectile : Projectile
{
    public ParticleSystem trail;

    void OnEnable()
    {
        Instantiate(trail);
        trail.Play();

        speed = AcornManager.Instance.speed;
        activeTime = AcornManager.Instance.activeTime;

        if (AcornManager.Instance.isBouncy)
        {
            GetComponent<Rigidbody2D>().sharedMaterial.friction = 0;
            GetComponent<Rigidbody2D>().sharedMaterial.bounciness = 1;
        }
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
                if (AcornManager.Instance.isBouncy)
        {
            GetComponent<Rigidbody2D>().sharedMaterial.friction = 0;
            GetComponent<Rigidbody2D>().sharedMaterial.bounciness = 1;
        }
    }

    public override void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag.Contains("enemy"))
        {
            Enemy enemy = collision.gameObject.GetComponent<Enemy>();
            enemy.TakeDamage(damage);
        }

        if (!AcornManager.Instance.isBouncy)
        {
            PlayParticles();
            Destroy(gameObject);
        }
    }
}
