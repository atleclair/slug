﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ShootableObject : MonoBehaviour
{
    [SerializeField] protected float speed = 10.0f;
    [SerializeField] protected float damage = 1;
    [SerializeField] protected float activeTime = 1.0f;
    [SerializeField] protected float screenShakeAmount = 0.01f;
    [SerializeField] protected ParticleSystem particles;

    public virtual void OnCollisionEnter2D(Collision2D collision)
    {
        Instantiate(particles, collision.transform.position, Quaternion.identity);
        particles.Play();
    }

    public virtual void Shoot(Vector3 direction)
    {
        GetComponent<Rigidbody2D>().velocity = direction.normalized * speed;
        StartCoroutine(DestroyProjectile());
    }
    
    private IEnumerator DestroyProjectile()
    {
        yield return new WaitForSeconds(activeTime);
        Destroy(gameObject);
    }
}
