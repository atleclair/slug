﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Throw : MonoBehaviour
{
    public TimeManager timeManager;
    public GameObject player;

    // direction to shoot projectile
    public Vector3 direction;
    // where the player's thumb was placed on the screen
    public Vector3 pivotPoint;

    [SerializeField] private float distance_DontShoot;
    [SerializeField] private float distance_RegularAbility;

    private float originalPenaltyTime;
    public float penaltyTime;
    private float penaltyTimeStep;
    private float timeOfPenalty;

    private bool pivotPointSet;
    private Jump playerJump;
    private float distanceFromPivot;
    Vector3 mousePos;

    private int specialAbilityCode;

    public List<Projectile> projectiles = new List<Projectile>();
    public AcornProjectile projectile;

    public GameObject indicator;
    private GameObject indic;
    private float indicatorDistance;
    private Quaternion rotation;

    // angular speed in raidans per sec
    public float speed = 1f;
    float singleStep;

    // flags for weapon upgrades
    private bool first_weaponUpgraded = false; 
    private bool second_weaponUpgraded = false;

    private bool invertedControl;


    void Start()
    {
        player = GameManager.sharedInstance.player;
        playerJump = GetComponent<Jump>();
        pivotPointSet = false;

        GameEvents.instance.OnOrange2 += UpgradeWeapon;
        GameEvents.instance.OnPlayerStopAttack += ResetAttack;

        // create and hide indicator
        indic = Instantiate(indicator, transform.position, Quaternion.identity);
        IndicateDirection(false);
        indicatorDistance = 1;

        originalPenaltyTime = 0f;
        penaltyTime = originalPenaltyTime;
        penaltyTimeStep = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        mousePos = Input.mousePosition;
        singleStep = speed * Time.deltaTime;

        // player is off the wall and is pressing down on screen and has at least 1 energy available
        if (InputHandler.instance.TouchDown() && !playerJump.isOnWall && EnergyManager.Instance.CanUseEnergy())
        {
            if (!IsPenalized())
            {
                SlowTime_SetPivotPoint();
            }
        }

        // while player's thumb is down, draw a ray to show what the shot will collide with
        if (pivotPointSet)
        {
            Prepare();
        }


        // when player lifts thumb, retrieve the final ray hit
        if (InputHandler.instance.TouchUp() && timeManager.timeSlowed)
        {
            Attack();
            timeManager.SlowmoOff();
            ResetAttack();
        }
    }

    private void SlowTime_SetPivotPoint()
    {
        // create the pivot point for the player's thumb to circle around
        // if it hasn't been created already
        if (!pivotPointSet)
        {
            pivotPoint = Input.mousePosition;
            pivotPointSet = true;
        }

        timeManager.SlowDownWithDuration(.1f);
    }

    private void Prepare()
    {
        // TODO: Add as option
        // if (invertedControl) direction = (mousePos - pivotPoint);
        // if (!invertedControl) direction = (pivotPoint - mousePos);

        direction = (pivotPoint - mousePos);
        
        distanceFromPivot = Vector3.Distance(pivotPoint, mousePos);

        if (distanceFromPivot <= distance_DontShoot) 
        {
            specialAbilityCode = 0;
            EnergyManager.Instance.UnhighlightEnergy();
        }

        if (distanceFromPivot > distance_DontShoot )
        {
            specialAbilityCode = 1;
            EnergyManager.Instance.HighlightEnergy(EnergyManager.Instance.GetEnergy().index);

            IndicateDirection(true);
        } 
    }

    private void IndicateDirection(bool active)
    {   
        // activate pointer
        indic.SetActive(active);
        indic.transform.position = GameManager.sharedInstance.player.transform.position + (direction.normalized * indicatorDistance);

        // speed up animation beacuse shooting is in slowmo
        indic.GetComponent<Animator>().speed = 5;

        // rotate pointer to face direction of shot
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        indic.transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    
    }

    private void Attack()
    {
        
        switch (specialAbilityCode)
        {
            // player pressed by accident
            // penalize player
            case 0:
                Penalize(true);
                break;

            // regular attack
            case 1:
                EnergyManager.Instance.UseEnergy();
                if (AcornManager.Instance.shotgunEnabled)
                {
                    ShotgunProjectile(projectile);
                }
                else 
                {
                    ShootProjectile(projectile);
                }
                AudioManager.Instance.Play("shoot");
                Penalize(false);
                break;
        }
    }

    // Regular Attack
    // Shoots single bullet straight
    private void ShootProjectile(ShootableObject projectile)
    {
        projectile = Instantiate(projectile, player.transform.position, Quaternion.identity);
        projectile.Shoot(direction);
    }

    // Special Attack
    // Shoots 3 bullets in a fan pattern
    private void ShotgunProjectile(Projectile projectile)
    {
        float shotgunSpread = 75f;

        Projectile p1 = Instantiate(projectile, player.transform.position, Quaternion.identity);
        Projectile p2 = Instantiate(projectile, player.transform.position, Quaternion.identity);
        Projectile p3 = Instantiate(projectile, player.transform.position, Quaternion.identity);

        float xLeft = direction.x - shotgunSpread;
        float xRight = direction.x + shotgunSpread;

        Vector3 direction_left = new Vector3(xLeft, direction.y, direction.z);
        Vector3 directon_right = new Vector3(xRight, direction.y, direction.z);

        p1.Shoot(direction);
        p2.Shoot(direction_left);
        p3.Shoot(directon_right);

    }

    // enables the first weapon upgrade 
    private void UpgradeWeapon(string powerup)
    {
        if (!first_weaponUpgraded)
        {
            first_weaponUpgraded = true;
            return;
        }

        if (!second_weaponUpgraded)
        {
            second_weaponUpgraded = true;
            return;
        }

    }

    private void ResetAttack()
    {
        // remove aiming indicator
        IndicateDirection(false);

        EnergyManager.Instance.UnhighlightEnergy();
        
        pivotPointSet = false;
    }

    // incrementally punishes player up to point to prevent shooting if they continue
    // to press down but not shoot
    private void Penalize(bool shouldPenalize)
    {
        int maxPenaltyTime = 3;

        if (shouldPenalize)
        {
            timeOfPenalty = Time.time;
            penaltyTime = Mathf.Min(penaltyTime += penaltyTimeStep, maxPenaltyTime);
        }

        if (!shouldPenalize)
        {
            penaltyTime = originalPenaltyTime;
        }
    }

    public bool IsPenalized()
    {
        if (Time.time - timeOfPenalty >= penaltyTime)
        {
            return false;
        }

        return true;
    }

}
