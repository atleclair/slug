﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowTeleport : MonoBehaviour {

    public Jump playerJump;
    public TimeManager timeManager;
    public ScreenShake screenShake;
    public float speed = 50;
    public float momentumMinimum;
    private Vector3 playerAimPos;
    private bool isInSlowmo;
    private bool isFlinging;
    [SerializeField] Momentum momentum;
    
    
	// Use this for initialization
	void Start () {
        momentumMinimum = 75;
	}
	
	// Update is called once per frame
	// void Update () {
    //     playerAimPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
	// 	if (!playerJump.isOnWall && Input.GetMouseButtonDown(0) )
    //     {
    //         timeManager.SlowmoOn();
    //         isInSlowmo = true;
    //     }
    //     if (Input.GetMouseButtonUp(0) && isInSlowmo)
    //     {
    //         timeManager.SlowmoOff();
    //         //StartCoroutine(FlingPlayer());
    //     }
    // }



    IEnumerator FlingPlayer()
    {
        yield return new WaitForSeconds(.05f);
        Vector2 direction = playerAimPos - transform.position;
        //direction.Normalize();
        GetComponent<Rigidbody2D>().velocity = direction * speed;
        isInSlowmo = false;
        isFlinging = true;
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (isFlinging)
        {
            isFlinging = false;
        }
    }


    void ShootProjectile()
    {

    }
}
