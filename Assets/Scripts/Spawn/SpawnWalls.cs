﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnWalls : MonoBehaviour
{
    public GameObject wallTile;

    public static SpawnWalls SharedInstance;
    bool isDefaultOffsetApplied = false;
    bool leftPerimeterSet = false;
    bool rightPerimeterSet = false;

    Vector2 leftSpawnPoint;
    Vector2 rightSpawnPoint;
    Vector3 upperRightCorner;

    Vector3 leftPerim;
    Vector3 rightPerim;

    float leftPerimeter;
    float rightPerimeter;

    private float screenHeight;
    private float wallWidth;

    private float playerHeight;
    private bool playerHeightReady;

    void Start()
    {
        SharedInstance = this;

        screenHeight = ScreenBoundsManager.Instance.topOfScreen_Y;

        // The bottom and right corners of the screen. Will need to add additional offset
        // to spawn walls inside the screen
        leftSpawnPoint = ScreenBoundsManager.Instance.bottomLeftCorner_world;
        rightSpawnPoint = ScreenBoundsManager.Instance.bottomRightCorner_world;

        playerHeight = 0;
        StartCoroutine(SetPlayerHeight());

        SpawnWall();
    }

    private void Update()
    {
        upperRightCorner = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
        if (playerHeightReady) playerHeight = Player.Instance.transform.position.y;
        SpawnWall();
    }

    // ensures that the player is instantiated in the game before calling on its height
    IEnumerator SetPlayerHeight()
    {
        yield return new WaitForSeconds(10);
        playerHeight = Player.Instance.transform.position.y;
        playerHeightReady = true;
    }

    public void SpawnWall()
    {
        while (leftSpawnPoint.y < upperRightCorner.y + screenHeight / 2)
        {
            GameObject leftWallTile = ChooseTile();
            GameObject rightWallTile = ChooseTile();

            // wall spawns at middle of object, need to move it by half the height and width
            Vector2 wallExtents = new Vector2(leftWallTile.GetComponent<SpriteRenderer>().bounds.extents.x,
                                  leftWallTile.GetComponent<SpriteRenderer>().bounds.extents.y);
            Vector2 rightWallExtents = new Vector2(rightWallTile.GetComponent<SpriteRenderer>().bounds.extents.x,
                                       leftWallTile.GetComponent<SpriteRenderer>().bounds.extents.y);

            // flip right wall tile backwards
            rightWallTile.transform.localScale = new Vector3(-rightWallTile.transform.localScale.x, rightWallTile.transform.localScale.y, rightWallTile.transform.localScale.z);

            leftWallTile.transform.position = leftSpawnPoint + wallExtents;

            if (!leftPerimeterSet)
            {
                leftPerim = leftWallTile.transform.position;
                leftPerim.x += leftWallTile.GetComponent<SpriteRenderer>().bounds.extents.x;
                leftPerimeterSet = true;
            }

            rightWallTile.transform.position = rightSpawnPoint - rightWallExtents;
            if (!rightPerimeterSet)
            {
                rightPerim = rightWallTile.transform.position;
                rightPerim.x -= rightWallTile.GetComponent<SpriteRenderer>().bounds.extents.x;
                rightPerimeterSet = true;
            }

            // places next tile one wall tile height away
            leftSpawnPoint.y += leftWallTile.GetComponent<Renderer>().bounds.size.y;
            rightSpawnPoint.y += leftWallTile.GetComponent<Renderer>().bounds.size.y;
        }
    }


    /* private WallTile GetTile()
     * {
     *      if (player.height > etc.)
     *      { return tile );
     * 
     * 
     * }
     */

    public float GetWallWidth()
    {
        return wallWidth; 
    }

    public List<float> GetPlayArea()
    {
        List<float> perimeter = new List<float>();
        perimeter.Add(leftPerim.x);
        perimeter.Add(rightPerim.x);
        return perimeter;
    }

    GameObject ChooseTile()
    {
        float maxRoll = 30;

        float enemyTileSpawnChance_easy = maxRoll - 2;
        float enemyTileSpawnChance_hard = maxRoll - 4;

        float enemyTileRoll = Random.Range(0, maxRoll);

        if (playerHeight >= 0)
        {
            if (playerHeight >= 0 && playerHeight < 50) return ObjectPooler.Instance.Retrieve("wallTile");

            // use lower spawn chance for enemy tile
            if (playerHeight >= 50 && playerHeight < 300)
                {
                    if (enemyTileRoll >= enemyTileSpawnChance_easy)
                    {
                        return ObjectPooler.Instance.Retrieve("enemyTile");
                    } else
                    {
                        return ObjectPooler.Instance.Retrieve("wallTile");
                    }
                }

            // use higher chance to spawn enemy tile
            if (playerHeight >= 300)
            {
                if (enemyTileRoll >= enemyTileSpawnChance_hard)
                {
                    return ObjectPooler.Instance.Retrieve("enemyTile");
                } else
                {
                    return ObjectPooler.Instance.Retrieve("wallTile");
                }
            } 
        }
        return null;
    }

}
