﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnGrapplingPoints : MonoBehaviour
{
    public static SpawnGrapplingPoints instance;
    public GameObject player;
    public GameObject grapplingPoint;
    public List<GameObject> grapplingPoints = new List<GameObject>();
    int numGrapplingPointsSet;
    public int numGrapplingPointsPerScreen;
   
    private float maxDistance;
    public GameObject closestGrapplingPoint;

    // Use this for initialization
    void Start()
    {
        if (instance == null)
        {
            instance = this;
            //DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        maxDistance = Vector3.Distance(GameManager.sharedInstance.lowerLeft, GameManager.sharedInstance.upperLeft)/3;
        closestGrapplingPoint = null;
    }

    // Update is called once per frame
    void Update()
    {

        if (numGrapplingPointsSet < numGrapplingPointsPerScreen)
        {
            GameObject point = Instantiate(grapplingPoint) as GameObject;
            float x = Random.Range(GameManager.sharedInstance.lowerLeft.x, GameManager.sharedInstance.lowerRight.x);
            float y = Random.Range(GameManager.sharedInstance.lowerLeft.y, GameManager.sharedInstance.upperLeft.y);
            point.transform.position = new Vector2(x, y);
            grapplingPoints.Add(point);
            numGrapplingPointsSet++;
        }

        foreach (var point in grapplingPoints)
        {
            float distanceFromNewPoint = Vector3.Distance(point.transform.position, player.transform.position);
            if (distanceFromNewPoint < maxDistance && point.transform.position.y > player.transform.position.y)
            {
                if (closestGrapplingPoint != null)
                {
                    if (distanceFromNewPoint < Vector3.Distance(closestGrapplingPoint.transform.position, player.transform.position))
                    {
                        SwapPoints(point);
                    }
                }
                else SwapPoints(point);
            }
        }
    }

    private void SwapPoints(GameObject newPoint)
    {
        if (closestGrapplingPoint != null)
        {
            closestGrapplingPoint.GetComponent<SpriteRenderer>().color = Color.black;
        }
        newPoint.GetComponent<SpriteRenderer>().color = Color.white;
        closestGrapplingPoint = newPoint;
    }
}