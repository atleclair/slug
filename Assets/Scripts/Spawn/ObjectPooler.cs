﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour {

    public static ObjectPooler Instance;

    public List<GameObject> toPool = new List<GameObject>();
    private Dictionary<string, List<GameObject>> pool = new Dictionary<string, List<GameObject>>();
    private List<GameObject> activeObjects = new List<GameObject>();

    private int amountToPool = 150;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        else
        {
            Destroy(gameObject);
        }

        InitPool();
    }

    private void Start()
    {
        GameEvents.instance.OnSpawnReady += CheckInactive;
    }

    // Prepares object pool 
    private void InitPool()
    {
        List<GameObject> tempList;

        for (int i = 0; i < toPool.Count; i++)
        {
            string key = toPool[i].gameObject.tag;

            if (!pool.ContainsKey(key))
            {
                pool.Add(key, new List<GameObject>());
            }

            tempList = pool[toPool[i].gameObject.tag];

            for (int j = 0; j < amountToPool; j++)
            {
                GameObject go = Instantiate(toPool[i], transform.position, Quaternion.identity);
                go.SetActive(false);
                tempList.Add(go);
            }
        }
    }


    // Retrieve pooled object by tag name
    public GameObject Retrieve(string key)
    {
        if (pool.ContainsKey(key))
        {
            List<GameObject> poolList = pool[key];

            if (poolList != null)
            {
                for (int i = 0; i < poolList.Count; i++)
                {
                    if (!poolList[i].activeInHierarchy)
                    {
                        GameObject toReturn = poolList[i];
                        toReturn.SetActive(true);
                        activeObjects.Add(toReturn);
                        return toReturn;
                    }
                }

                Debug.Log("No more pooled items left in " + key);
                return null;
            }
        }

       Debug.Log(key + " not found in Dictionary.");
       return null;

    }


    public void Deactivate(GameObject obj)
    {
        activeObjects.Remove(obj);
        PrepareForReuse(obj);
        obj.SetActive(false);
    }

    public void PrepareForReuse(GameObject toReuse)
    {
        if (toReuse.GetComponent<Poolable>() != null)
        {
            toReuse.GetComponent<Poolable>().Reset();
        }

        if (toReuse.gameObject.transform.localScale.x < 0)
        {
            toReuse.gameObject.transform.localScale = new Vector3(-toReuse.transform.localScale.x, toReuse.transform.localScale.y, toReuse.transform.localScale.z);
        }
    }

    private void CheckInactive()
    {
        Queue<GameObject> toRemove = new Queue<GameObject>();

        for (int i = 0; i < activeObjects.Count; i++)
        {
            if (activeObjects[i].transform.position.y < ScreenBoundsManager.Instance.bottomOfScreen_Y)
            {
                toRemove.Enqueue(activeObjects[i]);
            }
        }
            foreach(GameObject obj in toRemove)
            { 
                Deactivate(obj);
            }
    }


}
