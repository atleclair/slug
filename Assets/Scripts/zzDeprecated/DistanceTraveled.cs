﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class DistanceTraveled : MonoBehaviour
{

    public static DistanceTraveled Instance;

    [SerializeField]
    private TextMeshProUGUI distanceText;

    public float distance;

    private float distanceTraveled;
    private float startingDistance;
    private float currDistance;


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        startingDistance = GameManager.sharedInstance.player.transform.position.y;
        distanceTraveled = 0;
        currDistance = distanceTraveled;

        distanceText.text = 0.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        distanceTraveled = GameManager.sharedInstance.player.transform.position.y - startingDistance;
        if (distanceTraveled > 0 && distanceTraveled > currDistance)
        {
            distanceText.text = Math.Round(distanceTraveled).ToString();
            currDistance = distanceTraveled;
            distance = distanceTraveled;
        }
    }
}
