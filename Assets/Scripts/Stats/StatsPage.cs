﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StatsPage : MonoBehaviour
{
    public TextMeshProUGUI highScore;
    public TextMeshProUGUI totalDistance;
    public TextMeshProUGUI slugPoints;

    // Start is called before the first frame update
    void Start()
    {
        highScore.text = PlayerPrefs.GetInt("highScore").ToString();
        totalDistance.text = PlayerPrefs.GetFloat("totalDist").ToString();
        slugPoints.text = PlayerPrefs.GetInt("slugPoints").ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DeleteData()
    {
        PlayerPrefs.DeleteAll();

        highScore.text = PlayerPrefs.GetInt("highScore").ToString();
        totalDistance.text = PlayerPrefs.GetFloat("totalDist").ToString();
        slugPoints.text = PlayerPrefs.GetInt("slugPoints").ToString();
    }
}
