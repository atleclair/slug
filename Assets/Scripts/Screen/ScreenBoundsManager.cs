﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenBoundsManager : MonoBehaviour
{
    public static ScreenBoundsManager Instance;

    public float wantedAspectRatio = (float) 9 / 16;

    private float screenHeight;

    private float screenWidth;
    private float adjustedScreenWidth;

    private float screenCenter;
    

    private bool isNativeAspect;

    // represents the current top and bottom of the screen's Y coord
    public float bottomOfScreen_Y;
    public float topOfScreen_Y;

    public float verticalScreenDistance;
    public float horizontalScreenDistance;

    // the playable area that should be fair game to spawn things in
    // | |-----------| |
    // | |-----------| |
    public float spawnableArea_leftPerimeter;
    public float spawnableArea_rightPerimeter;

    public bool pointsInitialized = false;

    // the current corners of the screen in world coordinates
    public Vector3 upperLeftCorner_world;
    public Vector3 bottomLeftCorner_world;
    public Vector3 upperRightCorner_world;
    public Vector3 bottomRightCorner_world;

    public GameObject wallTile;

    List<float> perimeter = new List<float>();

    void Awake()
    {

        if (Instance == null) Instance = this;
        else { Destroy(gameObject); }
        
        screenHeight = Screen.height;
        screenWidth = Screen.width;

        isNativeAspect = true;

        Setup();
        InitPoints();

        spawnableArea_leftPerimeter = bottomLeftCorner_world.x + wallTile.GetComponent<SpriteRenderer>().bounds.extents.x;
        spawnableArea_rightPerimeter = bottomRightCorner_world.x - wallTile.GetComponent<SpriteRenderer>().bounds.extents.x;

        verticalScreenDistance = topOfScreen_Y - bottomOfScreen_Y;
        horizontalScreenDistance = spawnableArea_rightPerimeter - spawnableArea_leftPerimeter;
    }


    void Start()
    {

    }

    void Setup()
    {
        float currentAspectRatio = (float)Screen.width / Screen.height;
        screenCenter = (float)Screen.width / 2;

        if (currentAspectRatio > wantedAspectRatio)
        {
            adjustedScreenWidth = (float)Screen.height * wantedAspectRatio;
            isNativeAspect = false;
        }

        if (currentAspectRatio < wantedAspectRatio)
        {
            adjustedScreenWidth = (float)Screen.height * wantedAspectRatio;
            isNativeAspect = false;
        }
    }

    void Update()
    {
        InitPoints();
    }

    private void InitPoints()
    {
        if (isNativeAspect)
        {
            upperLeftCorner_world = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height, 0));
            bottomLeftCorner_world = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0));

            upperRightCorner_world = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
            bottomRightCorner_world = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0));
        }

        else 
        {
            upperLeftCorner_world = Camera.main.ScreenToWorldPoint(new Vector3(screenCenter - adjustedScreenWidth / 2,  Screen.height, 0));
            bottomLeftCorner_world = Camera.main.ScreenToWorldPoint(new Vector3(screenCenter - adjustedScreenWidth / 2, 0, 0));

            upperRightCorner_world = Camera.main.ScreenToWorldPoint(new Vector3(screenCenter + adjustedScreenWidth / 2, Screen.height, 0));
            bottomRightCorner_world = Camera.main.ScreenToWorldPoint(new Vector3(screenCenter + adjustedScreenWidth / 2, 0, 0));
        }

        bottomOfScreen_Y = bottomLeftCorner_world.y;
        topOfScreen_Y = upperLeftCorner_world.y;

        pointsInitialized = true;
    }
}

