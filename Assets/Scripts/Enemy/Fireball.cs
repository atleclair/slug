﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    [SerializeField] public float fireballSpeed;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "player")
        {
            // damage the player
        }

        if (collision.gameObject.tag == "enemy") return;

        else
        {
            // play particle effect 
        }
    }
}
