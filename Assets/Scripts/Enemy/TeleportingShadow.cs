﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportingShadow : MonoBehaviour
{
    Animator animator;
    CircleCollider2D collider;
    [SerializeField] private float minTimeBetweenDisappear;
    [SerializeField] private int weight; // a number between 1-10, higher number means more likely to disappear
    [SerializeField] private int retrySeconds; // number of seconds between retrying on unsuccessful disappearance
    private float timeOfDisappearance;
    private bool isPreparing;

    float lowerLeftX;
    float lowerRightX;
    float lowerLeftY;
    float upperLeftY;

    // Start is called before the first frame update

    private void Awake()
    {

    }
    void Start()
    {
        animator = GetComponent<Animator>();
        collider = GetComponent<CircleCollider2D>();
        timeOfDisappearance = Time.time;

        lowerLeftX = GameManager.sharedInstance.lowerLeft.x;
        lowerRightX = GameManager.sharedInstance.lowerRight.x;
        lowerLeftY = GameManager.sharedInstance.lowerLeft.y;
        upperLeftY = GameManager.sharedInstance.upperLeft.y;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - timeOfDisappearance >= minTimeBetweenDisappear && !isPreparing)
        {
            StartCoroutine(CheckDisappear());
        }
    }

    private IEnumerator CheckDisappear()
    {
        isPreparing = true;
        int randomRoll = Random.Range(1, 11);
        while (randomRoll >= weight)
        {
            yield return new WaitForSeconds(retrySeconds);
            randomRoll = Random.Range(1, 11);
        }
        isPreparing = false;
        Disappear();
        yield break;
        
    }

    private void Disappear()
    {
        timeOfDisappearance = Time.time;
        collider.enabled = false;
        animator.SetBool("isDisappearing", true);
        AnimatorClipInfo[] currentClipInfo = animator.GetCurrentAnimatorClipInfo(0);
        float currentClipLength = currentClipInfo[0].clip.length;
        StartCoroutine(Reappear(currentClipLength));
    }

    private IEnumerator Reappear(float clipLength)
    {
        yield return new WaitForSeconds(clipLength);
        animator.SetBool("isDisappearing", false);

        float RandomXPos = Random.Range(lowerLeftX, lowerRightX);
        float RandomYPos = Random.Range(lowerLeftY, upperLeftY);

        transform.position = new Vector2(RandomXPos, RandomYPos);

        animator.SetBool("isReappearing", true);

        AnimatorClipInfo[] currentClipInfo = animator.GetCurrentAnimatorClipInfo(0);
        float currentClipLength = currentClipInfo[0].clip.length;
        StartCoroutine(Reappeared(currentClipLength));
    }

    private IEnumerator Reappeared(float clipLength)
    {
        yield return new WaitForSeconds(clipLength);
        animator.SetBool("isReappearing", false);
        collider.enabled = true;
    }


    public void Killed()
    {
        animator.SetTrigger("isDead");
    }
}
