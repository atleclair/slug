﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class Enemy : SpawnableObject
{
    [SerializeField] protected float hitPoints = 1;
    [SerializeField] protected float attackDamage = 1;
    [SerializeField] protected float moveSpeed = 1;
    [SerializeField] protected ParticleSystem particles;

    public Spirit spirit;

    public abstract void Attack();

    public abstract void Move();

    public float DealDamage()
    {
        return attackDamage;
    }

    public void TakeDamage(float dmg)
    {
        hitPoints -= dmg;
        if (hitPoints <= 0) 
        {
            AnalyticsEvents.Instance.EnemyKilled();
            OnDeath(); 
        }
    }

    public override void Setup()
    {
        spawnPadding = 2;
    }

    public virtual void OnDeath()
    {
        Instantiate(particles, transform.position, Quaternion.identity);
        Instantiate(spirit, transform.position, Quaternion.identity);
        ObjectPooler.Instance.Deactivate(gameObject);
    }

        void OnParticleCollision(GameObject other)
    {
        Debug.Log("collided");
    }

    public virtual void OnDisable()
    {
        GameEvents.instance.EnemyDeath();
    }
}

