﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyTrap : MonoBehaviour {

    public Animator animator;
    SpriteRenderer sr;
    public float resetTime;
    public float activationTime;
    float lastAttackTime;
    bool canActivate;
    GameObject player;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
        canActivate = true;
        player = GameManager.sharedInstance.player;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (canActivate)
        {
            StartCoroutine(Attack());
        }
    }

    IEnumerator Attack()
    {

        yield return new WaitForSeconds(activationTime);
        animator.SetBool("IsActivated", true);
        if (transform.position.x - player.transform.position.x < .2 && transform.position.y < .2 )
        {
            GameManager.sharedInstance.KillPlayer();
        }
        lastAttackTime = Time.time;
        canActivate = false;
    }

    private void Update()
    {
        if (Time.time - lastAttackTime >= resetTime)
        {
            canActivate = true;
        }
    }
}
