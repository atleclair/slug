﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : MonoBehaviour {

    public float speed;
    SpriteRenderer sr;
    Rigidbody2D rb;
    Vector2 screenBounds;

    float leftPatrolPoint;
    float rightPatrolPoint;

    bool patrolPaused = false;

	// Use this for initialization
	void Start () {
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        rb.velocity = transform.right * speed;
        leftPatrolPoint = Random.Range(-screenBounds.x, 0);
        rightPatrolPoint = Random.Range(0, screenBounds.x);
	}
	
	// Update is called once per frame
	void Update () {
        if (!patrolPaused)
        {
            PatrolPath(leftPatrolPoint, rightPatrolPoint);
        }
	}

    void PatrolPath(float x1, float x2)
    {
        if (rb.transform.position.x <= x1)
        {
            rb.velocity = transform.right * speed;
            sr.flipX = true;
        }

        if (rb.transform.position.x >= x2)
        {
            rb.velocity = -transform.right * speed;
            sr.flipX = false;
        }
    }

    public void PausePatrol()
    {
        patrolPaused = true;
        rb.velocity = new Vector2(0, 0);
    }

    public void ResumePatrol()
    {
        patrolPaused = false;
    }
}
