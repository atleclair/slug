﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportProjectile : MonoBehaviour
{
    private GameObject player;
    private Jump j;

    private void Awake()
    {
        player = GameManager.sharedInstance.player;
        j = GameManager.sharedInstance.player.GetComponent<Jump>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameManager.sharedInstance.player.transform.position = this.transform.position;
        Destroy(this.gameObject);
    }

}
