﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_ShootAllDirections : Enemy_Shoot4Directions
{
    public override void Attack()
    {
        if (this.gameObject.transform.position.y > ScreenBoundsManager.Instance.bottomOfScreen_Y &&
            this.gameObject.transform.position.y < ScreenBoundsManager.Instance.topOfScreen_Y)
        {
            AudioManager.Instance.Play("enemyShoot");
            Vector3[] directions = new Vector3[4];
            directions[0] = new Vector3(0, 100, 0);
            directions[1] = new Vector3(100, 0, 0);
            directions[2] = new Vector3(0, -100, 0);
            directions[3] = new Vector3(-100, 0, 0);
            Attack(directions);
        } 
        timeOfAttack = Time.time;
    }

    public void Attack(Vector3[] directions)
    {
        foreach (Vector3 dir in directions)
        {
            EnemyProjectile ep = Instantiate(projectile, transform.position, Quaternion.identity);
            ep.Shoot(dir);
        }
    }

    public new void Ready()
    {
        return;
    }
}
