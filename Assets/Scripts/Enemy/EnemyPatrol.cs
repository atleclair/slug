﻿using UnityEngine;

public class EnemyPatrol : MonoBehaviour
{
    [SerializeField] private float speed;
    private Rigidbody2D rb;
    private SpriteRenderer sr;
    private float leftPatrolPoint;
    private float rightPatrolPoint;
    private Vector3 screenBounds;

    private void Awake()
    {
        screenBounds = GameManager.sharedInstance.screenBounds;
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
    }

    
    void Start()
    {
        PatrolPath(leftPatrolPoint, rightPatrolPoint);
        rb.velocity = transform.right * speed;
        leftPatrolPoint = Random.Range(-screenBounds.x, 0);
        rightPatrolPoint = Random.Range(0, screenBounds.x);
    }

    private void Update()
    {
        Debug.Log("Patrol left: " + leftPatrolPoint);
        Debug.Log("Patrol right: " + rightPatrolPoint);
    }

    void PatrolPath(float x1, float x2)
    {
        if (rb.transform.position.x <= x1)
        {
            rb.velocity = transform.right * speed;
            sr.flipX = true;
        }

        if (rb.transform.position.x >= x2)
        {
            rb.velocity = -transform.right * speed;
            sr.flipX = false;
        }
    }
    
}
