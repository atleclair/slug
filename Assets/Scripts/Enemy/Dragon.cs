﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dragon : MonoBehaviour
{
    [SerializeField] private Fireball fireball;
    [SerializeField] private float attackDuration;
    [SerializeField] private float numWaves;
    [SerializeField] private float numFireballs;
    [SerializeField] private float timeBetweenAttacks;
    private float timer;
    private Vector2 direction;
    GameObject player;


    void Start()
    {
        player = GameManager.sharedInstance.player;
        timer = Time.time;
    }

    void Update()
    {
        Debug.Log("Timer: " + timer);
        if (Time.time - timer >= timeBetweenAttacks)
        {
            ShootFireballs();
            Debug.Log("Shooting Fireballs");
        }
    }

    private void ShootFireballs()
    {
        direction = player.transform.position - transform.position;
        float rotationOffset = 5;

        for (int i = 0; i < numFireballs; i++)
        {
            Fireball f = Instantiate(fireball, transform.position, transform.rotation) ;
            f.GetComponent<Rigidbody2D>().velocity = direction.normalized * fireball.fireballSpeed;
        }
        timer = Time.time;
    }
}
