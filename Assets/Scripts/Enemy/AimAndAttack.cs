﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class AimAndAttack : MonoBehaviour {

    Patrol patrol; // script that controls the patrol on enemy unit
    SpriteRenderer sr; // renderer on enemy unit
    Vector3 attackTarget; // location of player for enemy to charge toward
    float timeSinceLastAttack;

    public Animator animator;
    public GameObject player; // the player character
    public float attackSpeed; // speed of the charge
    public float enemyWaitTime; // length of time to wait after activation before charge
    public float maxAttackDuration;

    private float currentAttackStartTime;


    void Start () 
    {
        sr = GetComponent<SpriteRenderer>();
        patrol = GetComponent<Patrol>();
        player = GameManager.sharedInstance.player;
        animator = GetComponent<Animator>();
	}
	
	void Update () 
    {
        CheckDistance();
	}

    void CheckDistance()
    {
        if (Mathf.Abs(player.transform.position.y - sr.transform.position.y) <= 2 &&
            Mathf.Abs(transform.position.x - sr.transform.position.x) <= 1)
        {
            if (Time.time - timeSinceLastAttack > 5)
            {
                animator.SetBool("hasSeenPlayer", true);
                timeSinceLastAttack = Time.time;
                StartCoroutine(WaitTime());
            }  
        }
    }


    IEnumerator WaitTime()
    {
        patrol.PausePatrol();
        yield return new WaitForSeconds(enemyWaitTime);
        attackTarget = player.transform.position;
        animator.SetBool("hasSeenPlayer", false);
        animator.SetBool("isAttacking", true);

        currentAttackStartTime = Time.time;
        StartCoroutine(Attack(attackTarget));
        }


    IEnumerator Attack(Vector3 characterPos)
    {
        while (characterPos != sr.transform.position && Time.time - currentAttackStartTime <= maxAttackDuration)
        {
            transform.position = Vector2.MoveTowards(new Vector2(transform.position.x, transform.position.y), characterPos, attackSpeed * Time.deltaTime);
            yield return null;
        }
        animator.SetBool("isAttacking", false);
        patrol.ResumePatrol();
    }

    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //        if (collision.gameObject.tag == "player")
    //        {
    //            collision.gameObject.SetActive(false);
    //            GameManager.sharedInstance.GameOver();
    //        }
    //}




}
