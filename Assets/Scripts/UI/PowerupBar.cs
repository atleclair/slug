﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerupBar : MonoBehaviour
{
    public Slider pinkSlider;
    public Slider orangeSlider;
    public Slider blueSlider;

    public Image background;

    void Start()
    {
        GameEvents.instance.OnPowerupUpdate += UpdateBar;

        pinkSlider.maxValue = 0;
        pinkSlider.value = 0;

        blueSlider.maxValue = 0;
        blueSlider.value = 0;

        orangeSlider.maxValue = 0;
        orangeSlider.value = 0;

        // make background semi-transparent
        Color temp = background.color;
        temp.a = 0.25f;
        background.color = temp;
    }

    private void UpdateBar(string item)
    {
        int numCollected = PowerupManager.Instance.GetNumCollected(item);
        int powerupRank = PowerupManager.Instance.GetPowerupRank(item);
        int numRequired = PowerupManager.Instance.GetNumRequired(powerupRank);

        switch (item)
        {
            case "pink":
                pinkSlider.maxValue = numRequired;
                pinkSlider.value = numCollected;
                break;
            case "blue":
                blueSlider.maxValue = numRequired;
                blueSlider.value = numCollected;
                break;
            case "orange":
                orangeSlider.maxValue = numRequired;
                orangeSlider.value = numCollected;
                break;
        }
    }
}
