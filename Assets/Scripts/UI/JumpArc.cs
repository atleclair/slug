﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class JumpArc : MonoBehaviour {

    public static JumpArc sharedInstance;
    LineRenderer lr;
    public int resolution = 10;
    public float angle;
    float gravity; // force of gravity on y axis
    float radianAngle; 

    private void Awake()
    {
        if (sharedInstance == null)
        {
            sharedInstance = this;
            //DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        lr = GetComponent<LineRenderer>();
        gravity = Mathf.Abs(Physics2D.gravity.y);

    }

    // Use this for initialization
  //  void Start ()
  //  {
  //      RenderArc();
	 //}

    //private void OnValidate()
    //{
    //    if (lr != null && Application.isPlaying)
    //    {
    //        RenderArc();
    //    }
    //}

    // populating line renderer with appropriate settings
    public void RenderArc(float velocity)
    {
        lr.SetVertexCount(resolution + 1);
        lr.SetPositions(CalculateArcArray(velocity));
    }

    // create an array of Vector3 positions for arc
    Vector3[] CalculateArcArray(float velocity)
    {
        Vector3[] arcArray = new Vector3[resolution + 1];
        radianAngle = Mathf.Deg2Rad * angle;
        float maxDistance = ((velocity * velocity * Mathf.Sin(2 * radianAngle)) / gravity) - GetComponent<SpriteRenderer>().transform.position.x;
        //arcArray[0] = GetComponent<SpriteRenderer>().transform.position;
        for (int i = 0; i <= resolution; i++)
        {
            // evenly spaces points along arc
            float t = (float)i / (float)resolution;
            arcArray[i] = CalculateArcPoint(t, maxDistance, velocity);
        }
        return arcArray;
    }

    // calculate height and distance of each vertex in array
    Vector3 CalculateArcPoint(float t, float maxDistance, float velocity)
    {
        float x = t * maxDistance;
        float y = (x * Mathf.Tan(radianAngle)) - ((gravity * x * x) /
                  (2 * velocity * velocity * Mathf.Cos(radianAngle) * Mathf.Cos(radianAngle)));
        return new Vector2(x - GetComponent<SpriteRenderer>().transform.position.x, y + GetComponent<SpriteRenderer>().transform.position.y);
        //return new Vector3(x, y);
    }
}
