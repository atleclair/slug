﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ResultsScreen : MonoBehaviour
{

    public TextMeshProUGUI header_Traveled;
    public TextMeshProUGUI header_Collected;
    public TextMeshProUGUI header_Killed;
    public TextMeshProUGUI header_Earned;

    public TextMeshProUGUI text_Traveled;
    public TextMeshProUGUI text_Leaves;
    public TextMeshProUGUI text_Spirits;
    public TextMeshProUGUI text_Hearts;
    public TextMeshProUGUI text_Enemies;
    public TextMeshProUGUI text_Quest;

    public TextMeshProUGUI num_Traveled;
    public TextMeshProUGUI num_Leaves;
    public TextMeshProUGUI num_Spirits;
    public TextMeshProUGUI num_Hearts;
    public TextMeshProUGUI num_Enemies;
    public TextMeshProUGUI num_Quest;

    public TextMeshProUGUI points_Traveled;
    public TextMeshProUGUI points_Leaves;
    public TextMeshProUGUI points_Spirits;
    public TextMeshProUGUI points_Hearts;
    public TextMeshProUGUI points_Enemies;
    public TextMeshProUGUI points_Quest;

    public TextMeshProUGUI totalScore;

    private const int value_travel = 10;
    private const int value_leaf = 1;
    private const int value_spirit = 10;
    private const int value_heart = 20;
    private const int value_enemy = 5;
    private const int value_quest = 200;

    float score;

    public GameObject gameOverPanel;

    private float timeBetweenSections = .5f;
    private float timeBetweenScores = .25f;

    private bool showingResults;

    public Button playAgain;
    public Button menu;

    private List<TextMeshProUGUI> headers = new List<TextMeshProUGUI>();

    private List<TextMeshProUGUI>[] sections = new List<TextMeshProUGUI>[4];

    // Start is called before the first frame update
    void Start()
    {
        GameEvents.instance.OnGameOver += Display;
        InitHeaderList();
        InitSectionsList();
        HideText();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && showingResults)
        {
            timeBetweenScores = 0;
            timeBetweenSections = 0;
        }
    }

    void HideText()
    {
        header_Traveled.enabled = false;
        header_Collected.enabled = false;
        header_Killed.enabled = false;
        header_Earned.enabled = false;

        num_Traveled.enabled = false;
        num_Leaves.enabled = false;
        num_Spirits.enabled = false;
        num_Hearts.enabled = false;
        num_Enemies.enabled = false;
        //num_Quest.enabled = false;

        points_Traveled.enabled = false;
        points_Leaves.enabled = false;
        points_Spirits.enabled = false;
        points_Hearts.enabled = false;
        points_Enemies.enabled = false;
        //points_Quest.enabled = false;

        totalScore.enabled = false;

        playAgain.gameObject.SetActive(false);
        menu.gameObject.SetActive(false);
    }

    void InitHeaderList()
    {
        headers.Add(header_Traveled);
        headers.Add(header_Collected);
        headers.Add(header_Killed);
        headers.Add(header_Earned);
    }

    void InitSectionsList()
    {
        sections[0] = new List<TextMeshProUGUI>
        {
            num_Traveled,
            points_Traveled
        };

        sections[1] = new List<TextMeshProUGUI>
        {
            num_Leaves,
            points_Leaves,
            num_Spirits,
            points_Spirits,
            num_Hearts,
            points_Hearts
        };

        sections[2] = new List<TextMeshProUGUI>
        {
            num_Enemies,
            points_Enemies
        };

        sections[3] = new List<TextMeshProUGUI>
        {
            totalScore
        };
    }

    private void Display()
    {
        RetrieveData();
        StartCoroutine(ShowResults());
    }

    private void RetrieveData()
    {
        // Collect Analytics
        num_Traveled.text = (Analytics.Instance.distanceTraveled).ToString();
        num_Leaves.text = Analytics.Instance.numTotalLeavesCollected.ToString();
        num_Spirits.text = Analytics.Instance.numSpiritsCollected.ToString();
        num_Hearts.text = Analytics.Instance.numHeartsCollected.ToString();
        num_Enemies.text = Analytics.Instance.numEnemiesKilled.ToString();

        // Calculate Points
        points_Traveled.text = CalculateDistancePoints().ToString();
        points_Leaves.text = (Analytics.Instance.numTotalLeavesCollected * value_leaf).ToString();
        points_Spirits.text = (Analytics.Instance.numSpiritsCollected * value_spirit).ToString();
        points_Hearts.text = (Analytics.Instance.numHeartsCollected * value_heart).ToString();
        points_Enemies.text = (Analytics.Instance.numEnemiesKilled * value_enemy).ToString();

        score = CalculateDistancePoints() +
                      Analytics.Instance.numTotalLeavesCollected * value_leaf +
                      Analytics.Instance.numSpiritsCollected * value_spirit +
                      Analytics.Instance.numHeartsCollected * value_heart +
                      Analytics.Instance.numEnemiesKilled * value_enemy;

        GameEvents.instance.SlugPointsEarned(score);

        totalScore.text = score.ToString() + " SLUG POINTS";
        LogResults();
    }



    private IEnumerator ShowResults()
    {
        showingResults = true;
        yield return new WaitForSeconds(.5f);

        gameOverPanel.SetActive(true);

        for (int i = 0; i < headers.Count; i++)
        {
            yield return new WaitForSeconds(timeBetweenSections);
            AudioManager.Instance.Play("ding");
            headers[i].enabled = true;
            yield return new WaitForSeconds(timeBetweenSections);

            for (int j = 0; j < sections[i].Count; j++)
            {
                yield return new WaitForSeconds(timeBetweenScores);
                AudioManager.Instance.Play("thump");
                sections[i][j].enabled = true;
            }
        }

        playAgain.gameObject.SetActive(true);
        menu.gameObject.SetActive(true);
    }


    private int CalculateDistancePoints()
    {
        int pointValue = 2;
        int exponent = 0;

        // represents the amount of distance traveled that will be awarded
        // before going up to the next rung of exponent
        int pointChunk = 100;

        int dist = (int)Analytics.Instance.distanceTraveled;
        int distPoints = 0;

        while (dist > 0)
        {
            dist -= pointChunk;

            if (dist <= 0)
            {
                dist += pointChunk;
                distPoints += (int)(dist * (Mathf.Pow(pointValue, exponent)));
                dist = 0;
            }
            else
            {
                distPoints += (int)(pointChunk * (Mathf.Pow(pointValue, exponent)));
                exponent++;
            }
        }

        return distPoints;
    }

    private void LogResults()
    {
        // keys for data storage
        string highScore = "highScore";
        string totalDistance = "totalDist";
        string totalLeaves = "totalLeaves";
        string totalEnemies = "totalEnemies";
        string slugPoints = "slugPoints";


        //Dictionary<string, int> results = new Dictionary<string, int>();
        //results.Add("distance", (int)Analytics.Instance.distanceTraveled);
        //results.Add("leaves", (int)Analytics.Instance.numTotalLeavesCollected);
        //results.Add("slugPoints", (int)score);
        //GameEvents.instance.ResultsCalculated(results);

        // set high score
        if ((int)Analytics.Instance.distanceTraveled > PlayerPrefs.GetInt(highScore))
        {
            PlayerPrefs.SetInt(highScore, (int)Analytics.Instance.distanceTraveled);
        }

        // set total distance traveled
        float currTotalDistance = PlayerPrefs.GetFloat(totalDistance);
        currTotalDistance += Analytics.Instance.distanceTraveled;
        PlayerPrefs.SetFloat(totalDistance, currTotalDistance);

        // set total slug points
        int currSlugPoints = PlayerPrefs.GetInt(slugPoints);
        currSlugPoints += (int)score;
        PlayerPrefs.SetInt(slugPoints, currSlugPoints);
 
    }


}
