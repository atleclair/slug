﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    public static InputHandler instance;
    private bool isTouchingScreen;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            //DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        // check to see if player is touching screen
        if (Input.touchCount > 0) isTouchingScreen = true;
        else isTouchingScreen = false;
    }

    public bool TouchDown()
    {
        if (isTouchingScreen || Input.GetMouseButtonDown(0))
        {
            return true;
        }
        return false;
    }

    public bool TouchUp()
    {
        if (Input.GetMouseButtonUp(0))
        {
            return true;
        }
        return false;
    }

    //public bool SwipeUp()
    //{

    //}

}
