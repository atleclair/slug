﻿using UnityEngine;

public class SwipeDetector : MonoBehaviour
{
    private float startTime;
    private float endTime;
    private float swipeTime;
    private float maxTime;
    private float minSwipeDistance;
    [SerializeField] private float minHoldTime; // time until stationary finger is considered hold
    private float swipeDistance;
    private bool isStationary;

    private Vector2 directionOfSwipe;
    private Vector2 fingerDown;
    private Vector2 fingerUp;
    private Vector2 startPos;
    private Vector2 endPos;
    private Vector2 swipedDistance;
    private bool isSwiping;



    public bool detectSwipeOnlyAfterRelease = false;

    [SerializeField]
    private Jump playerJump;

    public float SWIPE_THRESHOLD = 20f;


    private void Update()
    {
        // if a touch has been detected, decide how to handle
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);

            switch (touch.phase)
            {
                // on first touch being detected, record starting position
                case TouchPhase.Began:
                    startTime = Time.time;
                    startPos = touch.position;
                    break;

                // if finger is still, consider it a hold
                case TouchPhase.Stationary:
                    isStationary = true;
                    float stationaryTime = Time.time;
                    if (Time.time - stationaryTime >= minHoldTime)
                    {
                        // freeze time mode
                    }
                    break;

                case TouchPhase.Moved:
                    if (touch.position.x > startPos.x) Debug.Log("Swiped right");
                    if (touch.position.x < startPos.x) Debug.Log("Swiped left");
                    break;
            }
        }
    }
}
