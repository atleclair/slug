﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{
    public GameObject background0; // the furthest to the back
    public GameObject background1;
    public GameObject background2;
    public GameObject background3; // the closest to the front

    public Sprite[] background0_sprites = new Sprite[5];
    public Sprite[] background1_sprites = new Sprite[5];
    public Sprite[] background2_sprites = new Sprite[5];
    public Sprite[] background3_sprites = new Sprite[5];


    // Start is called before the first frame update
    void Start()
    {
        background0.GetComponent<SpriteRenderer>().sortingOrder = 0;
        background1.GetComponent<SpriteRenderer>().sortingOrder = 1;
        background2.GetComponent<SpriteRenderer>().sortingOrder = 2;
        background3.GetComponent<SpriteRenderer>().sortingOrder = 3;

        GameEvents.instance.OnBiomeChange += SetBackground;

        SetBackground();

    }

    private void SetBackground()
    {
        int currBiome = ThresholdManager.Instance.currentBiome;

        background0.GetComponent<SpriteRenderer>().sprite = background0_sprites[currBiome];
        background1.GetComponent<SpriteRenderer>().sprite = background1_sprites[currBiome];
        background2.GetComponent<SpriteRenderer>().sprite = background2_sprites[currBiome];
        background3.GetComponent<SpriteRenderer>().sprite = background3_sprites[currBiome];

    }
}
