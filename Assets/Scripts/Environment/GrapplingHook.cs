﻿using UnityEngine;

public class GrapplingHook : MonoBehaviour {

    public Rigidbody2D rb;
    public DistanceJoint2D joint;
    public GameObject grapplingTarget;

    private bool isGrappling;

    private void Awake()
    {
        joint = GetComponent<DistanceJoint2D>();
        joint.enabled = false;

    }
    void Start () {
        
	}
	
	void Update () {
        if (isGrappling && Input.GetMouseButtonDown(0))
        {
            joint.distance = joint.distance - 5;
        }
    }

    public void GrappleToPoint(GameObject grapplingPoint)
    {
        joint.enabled = true;
        joint.connectedBody = grapplingPoint.GetComponent<Rigidbody2D>();
        joint.distance = Vector2.Distance(transform.position, grapplingPoint.transform.position);
        isGrappling = true;
    }




}
