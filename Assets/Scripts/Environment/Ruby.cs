﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ruby : SpawnableObject {

    float weight;
    int value;

	// Use this for initialization
	void Start () {
        weight = 5f;
        value = 2;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public override void Setup()
    {
        spawnPadding = 3;
    }
}
