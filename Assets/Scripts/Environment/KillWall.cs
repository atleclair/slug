﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillWall : MonoBehaviour {

    float timeStart;
    public float timeTilDestroy;
    bool hasTouched = false;
    bool isExploded;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        isExploded = false;
        timeStart = Time.time;
        hasTouched = true;
    }

    public void Update()
    {
        float newTime = Time.time;
        if (newTime - timeStart >= timeTilDestroy && hasTouched && !isExploded)
        {
            this.gameObject.SetActive(false);
            isExploded = true;
        }
    }
}
