﻿using UnityEngine;
using System.Collections;

public class ScreenShake : MonoBehaviour
{
    public static ScreenShake Instance;

    private Transform mtransform;
    // Desired duration of the shake effect
    private float shakeDuration = .01f;
    // A measure of magnitude for the shake. Tweak based on your preference
    private float shakeMagnitude = 0.1f;
    // A measure of how quickly the shake effect should evaporate
    private float dampingSpeed = 20.0f;
    // The initial position of the GameObject
    Vector3 initialPosition;

    private bool isShaking;

    private void Awake()
    {
        if (mtransform == null)
        {
            mtransform = GetComponent(typeof(Transform)) as Transform;
        }
    }

    private void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }

        else
        {
            Destroy(gameObject);
        }

        isShaking = false;
    }

    private void OnEnable()
    {
        initialPosition = transform.localPosition;
    }

    private void Update()
    {
        if (shakeDuration > 0)
        {
            isShaking = true;
            mtransform.localPosition = initialPosition + Random.insideUnitSphere * shakeMagnitude;

            shakeDuration -= Time.deltaTime * dampingSpeed;
        }

        else
        {
            isShaking = false;
            shakeDuration = 0f;
            shakeMagnitude = 0;
            transform.localPosition = initialPosition;
        }
    }

    public void TriggerShake(float shakeMag)
    {
        shakeDuration = 5.0f;
        shakeMagnitude = shakeMag;
    }

    public void TriggerShake(float shakeMag, float shakeDuration)
    {
        this.shakeDuration = shakeDuration;
        shakeMagnitude = shakeMag;
    }

    public void StopShake()
    {
        shakeDuration = 0f;
    }

    public bool ScreenShaking()
    {
        return isShaking;
    }
}