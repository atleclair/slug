﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyEnergy : MonoBehaviour {

    float spawnTime;
    float timeToActivate;
    Collider2D c;
	// Use this for initialization
	void Start () 
    {
        c = GetComponent<Collider2D>();
        c.enabled = false;
        spawnTime = Time.time;
        timeToActivate = 2.0f;
	}
	
	// Update is called once per frame
	void Update () 
    {
		if (Time.time - spawnTime > timeToActivate)
        {
            c.enabled = true;
        }
    }
}
