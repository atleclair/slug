﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteFirefly : MonoBehaviour
{
    GameObject player;

    Vector3 lowerLeftCorner;
    Vector3 upperRightCorner;

    [SerializeField] float timePaused;
    [SerializeField] float moveSpeed;

    float timeOfLastMove;
    private bool lastMoveComplete;

    // Start is called before the first frame update
    void Start()
    {
        player = GameManager.sharedInstance.player;
        lowerLeftCorner = GameManager.sharedInstance.lowerLeft;
        upperRightCorner = GameManager.sharedInstance.upperRight;
        timeOfLastMove = Time.time;
        lastMoveComplete = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - timeOfLastMove >= timePaused && lastMoveComplete)
        {
            float newPositionX = Random.Range(lowerLeftCorner.x, upperRightCorner.x);
            float newPositionY = Random.Range(lowerLeftCorner.y, upperRightCorner.y);
            Vector3 newPosition = new Vector3(newPositionX, newPositionY);
            timeOfLastMove = Time.time;
            StartCoroutine(MoveFirefly(newPosition));
        }
    }

    private IEnumerator MoveFirefly(Vector3 newPosition)
    {
        lastMoveComplete = false;
        while (transform.position != newPosition)
        {
            transform.position = Vector2.MoveTowards(new Vector2(transform.position.x, transform.position.y), newPosition, moveSpeed * Time.deltaTime);
            yield return null;
        }
        lastMoveComplete = true;
    }



    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "player")
        {
            Inventory.instance.CollectWhiteFirefly();
            Destroy(this.gameObject);
        }
    }
}
