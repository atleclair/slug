﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakObject : MonoBehaviour {

    private float timer;
    private bool isTouched = false;

    private void Update()
    {
        if (Time.time - timer >= 1 && isTouched)
        {
            float b = Time.time - timer;
            Destroy(this.gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        isTouched = true;
        timer = Time.time;
    }
}
