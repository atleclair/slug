﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectCoin : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        ManageScore.sharedInstance.AddPoints();
        ManageScore.sharedInstance.AddEnergy();
        Destroy(gameObject);
    }
}
