﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spirit : MonoBehaviour, Magnetizable
{

    private float collectionTime;
    private float spawnTime;
    private float healAmount;

    // Start is called before the first frame update
    void Start()
    {
        spawnTime = 1;
        GetComponent<CircleCollider2D>().enabled = false;
        healAmount = .33f;  
        UpdateAnimClipTimes();
        StartCoroutine(SpawnSpirit());
    }

    private IEnumerator SpawnSpirit()
    {
        yield return new WaitForSeconds(spawnTime);
        GetComponent<CircleCollider2D>().enabled = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag.Contains("player"))
        {
            AnalyticsEvents.Instance.SpiritCollected();
            AudioManager.Instance.Play("orange3");
            GameEvents.instance.HeartPickup(healAmount);
            GetComponent<CircleCollider2D>().enabled = false;
            StartCoroutine(Collected());
        }
    }

    private IEnumerator Collected()
    {
        GetComponent<Animator>().SetBool("isCollected", true);
        yield return new WaitForSeconds(collectionTime);
        Destroy(gameObject);
    }

    public void UpdateAnimClipTimes()
    {
        AnimationClip[] clips = GetComponent<Animator>().runtimeAnimatorController.animationClips;
        foreach(AnimationClip clip in clips)
        {
            switch(clip.name)
            {
                case "spirit_collected":
                    collectionTime = clip.length;
                    break;

                case "spirit_summon":
                    spawnTime = clip.length;
                    break;
            }
        }
    }

    public void MagnetizeToPlayer()
    {
        StartCoroutine(MagnetMove());
    }

    public IEnumerator MagnetMove()
    {
        while (transform.position != GameManager.sharedInstance.player.transform.position)
        {
            transform.position = Vector2.MoveTowards(
            new Vector2(transform.position.x, transform.position.y),
            GameManager.sharedInstance.player.transform.position, 4 * Time.deltaTime);
            yield return null;
        }
    }
}
