﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Analytics : MonoBehaviour
{
    public static Analytics Instance;

    public float numJumps;

    public float numPlayerHits;
    public float numEnemyHits;
    public float damageTaken;
    public float damageHealed;
    public int numEnemiesKilled;
    public float numAttacks;

    public float numItemsCollected;

    public float numTotalLeavesCollected;
    public float numGreenLeavesCollected;
    public float numOrangeLeavesCollected;
    public float numPinkLeavesCollected;

    public float numHeartsCollected;
    public float numSpiritsCollected;

    public float distanceTraveled;

    private int wallsTouched;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        GameEvents.instance.OnGameOver += FinalCalculation;

        AnalyticsEvents.Instance.OnJump += Jump;
        AnalyticsEvents.Instance.OnPlayerHit += PlayerHit;

        AnalyticsEvents.Instance.OnEnemyHit += EnemyHit;
        AnalyticsEvents.Instance.OnEnemyKilled += EnemyKilled;

        AnalyticsEvents.Instance.OnSpiritCollected += SpiritCollected;
        AnalyticsEvents.Instance.OnHeartCollected += HeartCollected;

        AnalyticsEvents.Instance.OnItemCollected += ItemCollected;
    }

    void Instance_OnHeartCollected()
    {
    }


    private void Update()
    {
      //  FinalCalculation();
    }

    void Jump()
    {
        numJumps++;
    }

    void PlayerHit()
    {
        numPlayerHits++;
    }

    void EnemyHit()
    {
        numEnemyHits++;
    }

    void EnemyKilled()
    {
        numEnemiesKilled++;
    }

    void ItemCollected(CollectibleItem item)
    {
        switch (item.gameObject.tag)
        {
            case "leaf_pink":
                numPinkLeavesCollected++;
                break;

            case "leaf_green":
                numGreenLeavesCollected++;
                break;

            case "leaf_orange":
                numOrangeLeavesCollected++;
                break;
        }
    }

    void HeartCollected()
    {
        numHeartsCollected++;
    }

    void SpiritCollected()
    {
        numSpiritsCollected++;
    }

    public void FinalCalculation()
    {
        numTotalLeavesCollected = numPinkLeavesCollected + numGreenLeavesCollected + numOrangeLeavesCollected;
        distanceTraveled = Mathf.Round(DistanceTraveled.Instance.distance);
    }

}
