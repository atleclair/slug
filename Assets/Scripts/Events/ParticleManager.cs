﻿using UnityEngine;
using System.Collections;

public class ParticleManager : MonoBehaviour
{
    public static ParticleManager Instance;

    void Start()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    private void Play(ParticleSystem ps, Transform t)
    {

        ps.Play();
    }
}
