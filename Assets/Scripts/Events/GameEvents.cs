﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameEvents : MonoBehaviour
{
    public static GameEvents instance;

    void Awake()
    {
        instance = this;
    }

    // collectible item picked up
    public event Action<CollectibleItem> OnFireflyPickup;
    public void FireflyPickup(CollectibleItem item)
    {
        OnFireflyPickup?.Invoke(item);
    }

    // next page of objects ready to be spawned
    public event Action OnSpawnReady;
    public void ReadyToSpawn()
    {
        OnSpawnReady?.Invoke();
    }

    // player is taking damage
    public event Action<float> OnPlayerDamage;
    public void PlayerDamage(float damage)
    {
        OnPlayerDamage?.Invoke(damage);
    }

    // heart item picked up
    public event Action<float> OnHeartPickup;
    public void HeartPickup(float healAmount)
    {
        OnHeartPickup?.Invoke(healAmount);
    }

    // threshold for a powerup has been completed
    public event Action<string> OnPowerupUpdate;
    public void PowerupUpdate(string powerupName)
    {
        OnPowerupUpdate?.Invoke(powerupName);
    }

    // player has died
    public event Action OnPlayerDeath;
    public void PlayerDeath()
    {
        OnPlayerDeath?.Invoke();
    }

    // player is attacking
    public event Action OnPlayerAttack;
    public void PlayerAttack()
    {
        OnPlayerAttack?.Invoke();
    }

    // player attack has ended
    public event Action OnPlayerStopAttack;
    public void PlayerStopAttack()
    {
        OnPlayerStopAttack?.Invoke();
    }

    public event Action OnPlayerIdle;
    public void PlayerIdle()
    {
        OnPlayerIdle?.Invoke();
    }

    public event Action OnPlayerSliding;
    public void PlayerSliding()
    {
        OnPlayerSliding?.Invoke();
    }

    public event Action OnPlayerWindup;
    public void PlayerWindup()
    {
        OnPlayerWindup?.Invoke();
    }

    public event Action OnPlayerJump;
    public void PlayerJump()
    {
        OnPlayerJump?.Invoke();
    }

    public event Action OnPlayerDoubleJump;
    public void PlayerDoubleJump()
    {
        OnPlayerDoubleJump?.Invoke();
    }

    public event Action<List<List<Vector3>>> OnSpawnComplete;
    public void SpawnComplete(List<List<Vector3>> coords)
    {
        Debug.Log("SENDING");
        OnSpawnComplete?.Invoke(coords);
    }

    public event Action OnRequestSpawnCoordinates;
    public void RequestSpawnCoordinates()
    {
        Debug.Log("REQUESTING");
        OnRequestSpawnCoordinates?.Invoke();
    }

    public event Action OnSpiritPickup;
    public void SpiritPickup()
    {
        OnSpiritPickup?.Invoke();
    }

    public event Action OnBiomeChange;
    public void BiomeChange()
    {
        OnBiomeChange?.Invoke();
    }

    public event Action OnMagnetize;
    public void Magnetize()
    {
        OnMagnetize?.Invoke();
    }

    public event Action OnGameOver;
    public void GameOver()
    {
        OnGameOver?.Invoke();
    }

    public event Action<float> OnSlugPointsEarned;
    public void SlugPointsEarned(float numPoints)
    {
        OnSlugPointsEarned?.Invoke(numPoints);
    }

    public event Action<Dictionary<string, int>> OnResultsCalculated;
    public void ResultsCalculated(Dictionary<string, int> results)
    {
        OnResultsCalculated?.Invoke(results);
    }

    public event Action OnEnabledPowerup;
    public void EnablePowerup()
    {
        OnEnabledPowerup?.Invoke();
    }

    public event Action OnAcornPower_Distance;
    public void AcornPower_Distance()
    {
        OnAcornPower_Distance?.Invoke();
    }

    public event Action OnAcornPower_Bounce;
    public void AcornPower_Bounce()
    {
        OnAcornPower_Bounce?.Invoke();
    }

    public event Action OnAcornPower_Pierce;
    public void AcornPower_Pierce()
    {
        OnAcornPower_Pierce?.Invoke();
    }

    public event Action OnAcornPower_Explode;
    public void AcornPower_Explode()
    {
        OnAcornPower_Explode?.Invoke();
    }

    public event Action OnAcornPower_Shotgun;
    public void AcornPower_Shotgun()
    {
        OnAcornPower_Shotgun?.Invoke();
    }

    public event Action OnLeavesFull;
    public void LeavesFull()
    {
        OnLeavesFull?.Invoke();
    }

    public event Action OnLeavesEmpty;
    public void LeavesEmpty()
    {
        OnLeavesEmpty?.Invoke();
    }

    public event Action OnSapPickup;
    public void SapPickup()
    {
        OnSapPickup?.Invoke();
    }

    public event Action OnEnemyDeath;
    public void EnemyDeath()
    {
        OnEnemyDeath?.Invoke();
    }

    // powerup threshold has been completed
    public event Action<string> OnPink1;
    public event Action<string> OnPink2;
    public event Action<string> OnPink3;
    public event Action<string> OnPink4;
    public event Action<string> OnPink5;
    public event Action<string> OnPink6;

    public event Action<string> OnBlue1;
    public event Action<string> OnBlue2;
    public event Action<string> OnBlue3;
    public event Action<string> OnBlue4;
    public event Action<string> OnBlue5;
    public event Action<string> OnBlue6;

    public event Action<string> OnOrange1;
    public event Action<string> OnOrange2;
    public event Action<string> OnOrange3;
    public event Action<string> OnOrange4;
    public event Action<string> OnOrange5;
    public event Action<string> OnOrange6;

    // uses the name of a powerup and rank to trigger 
    // the event that correlates with it
    public void PowerupActivated(string powerupName)
    {
        switch (powerupName)
        {
            case "pink1":
                OnPink1?.Invoke(powerupName);
                break;
            case "pink2":
                OnPink2?.Invoke(powerupName);
                break;
            case "pink3":
                OnPink3?.Invoke(powerupName);
                break;
            case "pink4":
                OnPink4?.Invoke(powerupName);
                break;
            case "pink5":
                OnPink5?.Invoke(powerupName);
                break;
            case "pink6":
                OnPink6?.Invoke(powerupName);
                break;

            case "blue1":
                OnBlue1?.Invoke(powerupName);
                break;
            case "blue2":
                OnBlue2?.Invoke(powerupName);
                break;
            case "blue3":
                OnBlue3?.Invoke(powerupName);
                break;
            case "blue4":
                OnBlue4?.Invoke(powerupName);
                break;
            case "blue5":
                OnBlue5?.Invoke(powerupName);
                break;
            case "blue6":
                OnBlue6?.Invoke(powerupName);
                break;

            case "orange1":
                OnOrange1?.Invoke(powerupName);
                break;
            case "orange2":
                OnOrange2?.Invoke(powerupName);
                break;
            case "orange3":
                OnOrange3?.Invoke(powerupName);
                break;
            case "orange4":
                OnOrange4?.Invoke(powerupName);
                break;
            case "orange5":
                OnOrange5?.Invoke(powerupName);
                break;
            case "orange6":
                OnOrange6?.Invoke(powerupName);
                break;
        }
    }

}
