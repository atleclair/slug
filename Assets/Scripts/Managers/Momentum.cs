﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Momentum : MonoBehaviour
{
    private float momentum;
    private bool gainMomentum;
    public float originalMomentumModifier;
    private float momentumModifier;

    void Start()
    {
        momentum = 0;
        originalMomentumModifier = 10;
        momentumModifier = originalMomentumModifier;
    }

    void Update()
    {
        if (momentum > 100) momentum = 100;

        if (gainMomentum && momentum < 100) AddMomentum();
    }

    private void AddMomentum()
    {
        momentum += momentumModifier * Time.deltaTime;
    }

    public void SubtractMomentum(float amountToSubtract)
    {
        momentum -= amountToSubtract;
    }

    public float GetMomentum()
    {
        return momentum;
    }

    public void StartMomentum()
    {
        gainMomentum = true;
    }

    public void StartMomentumWithModifier(float newModifier)
    {
        momentumModifier = newModifier;
        StartMomentum();
    }

    public void StopMomentum()
    {
        momentumModifier = originalMomentumModifier;
        gainMomentum = false;
    }

    public void zeroMomentum()
    {
        momentum = 0;
    }


}
