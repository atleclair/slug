﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public static GameManager sharedInstance;
    public GameObject player;
    public Vector3 playerPos;
    public GameObject enemyEnergy;
    public Vector3 screenBounds;
    public Vector3 spawnableArea;

    [SerializeField] private BoxCollider2D leftPerimeterCollider;
    [SerializeField] private BoxCollider2D rightPerimeterCollider;
    [SerializeField] private BoxCollider2D bottomCollider;

    // Screens coordinate corner location
    Vector3 upperLeftScreen = new Vector3(0, Screen.height);
    Vector3 upperRightScreen = new Vector3(Screen.width, Screen.height);
    Vector3 lowerLeftScreen = new Vector3(0, 0);
    Vector3 lowerRightScreen = new Vector3(Screen.width, 0, 0);

    //Corner locations in world coordinates
    public Vector3 upperLeft;
    public Vector3 upperRight;
    public Vector3 lowerLeft;
    public Vector3 lowerRight;

    private bool isAlive;
    private List<GameObject> spawnedObjects;
    private Vector3 cameraPosWorld;
    private Renderer render;

    private float playerStartY;
    private float playerCurrY;

    // distance beneath the screen before the player is considered dead
    private float playerSlipDistance;
    
    [SerializeField] private float distanceClimbed;

        
    private void Awake()
    {
        spawnableArea = new Vector2(screenBounds.x * .33f, screenBounds.x * .66f);
        screenBounds = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));
        cameraPosWorld = Camera.main.GetComponent<Transform>().position;
        

        if (sharedInstance == null)
        {
            sharedInstance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        isAlive = true;

        bottomCollider.transform.position = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, 0));

        upperLeft = Camera.main.ScreenToWorldPoint(upperLeftScreen);
        upperRight = Camera.main.ScreenToWorldPoint(upperRightScreen);
        lowerLeft = Camera.main.ScreenToWorldPoint(lowerLeftScreen);
        lowerRight = Camera.main.ScreenToWorldPoint(lowerRightScreen);

        spawnedObjects = new List<GameObject>();

     
        Screen.SetResolution(1080, 1920, true);
        
    }

    public void Start()
    {
        playerStartY = player.transform.position.y;
        if (playerStartY < 0) { playerStartY = 0; }

        playerSlipDistance = .5f;

        GameEvents.instance.OnGameOver += GameOver;

        // ignore player and player projectile collision
        Physics2D.IgnoreLayerCollision(9, 10);
        // ignore enemy and enemy projectile collision
        Physics2D.IgnoreLayerCollision(11, 12);
        // ignore enemy and player projectile
        Physics2D.IgnoreLayerCollision(10, 12);
        // ignore player and magnet collision
        Physics2D.IgnoreLayerCollision(9, 13);
        // ignore enemy proj and collectible
        Physics2D.IgnoreLayerCollision(12, 14);
        // ignore player projectile collision themselves
        Physics2D.IgnoreLayerCollision(10, 10);
        // ignore enemy projectiles themselves
        Physics2D.IgnoreLayerCollision(12, 12);
        // player proj and magnet
        Physics2D.IgnoreLayerCollision(10, 13);
        // enemy proj and magnet
        Physics2D.IgnoreLayerCollision(12, 13);
        Physics2D.IgnoreLayerCollision(12, 15);

        AudioManager.Instance.PlayAndLoop("MainTheme");
    }

    private void Update()
    {
        if (!ScreenShake.Instance.ScreenShaking())
        {
            screenBounds = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));
            cameraPosWorld = Camera.main.GetComponent<Transform>().position;

            upperLeftScreen = new Vector3(0, Screen.height);
            upperRightScreen = new Vector3(Screen.width, Screen.height);
            lowerLeftScreen = new Vector3(0, 0);
            lowerRightScreen = new Vector3(Screen.width, 0, 0);

            //Corner locations in world coordinates
            upperLeft = Camera.main.ScreenToWorldPoint(upperLeftScreen);
            upperRight = Camera.main.ScreenToWorldPoint(upperRightScreen);
            lowerLeft = Camera.main.ScreenToWorldPoint(lowerLeftScreen);
            lowerRight = Camera.main.ScreenToWorldPoint(lowerRightScreen);

            playerPos = player.transform.position;

            MovePerimeters();
        }

        //if (playerPos.y < ScreenBoundsManager.Instance.bottomOfScreen_Y - playerSlipDistance && isAlive)
        //{
        //    isAlive = false;
        //    GameEvents.instance.GameOver();

        //}

    }

    private void MovePerimeters()
    {
        leftPerimeterCollider.transform.position = new Vector3(ScreenBoundsManager.Instance.bottomLeftCorner_world.x + leftPerimeterCollider.size.x, screenBounds.y);
        rightPerimeterCollider.transform.position = new Vector3(ScreenBoundsManager.Instance.bottomRightCorner_world.x - rightPerimeterCollider.size.x, screenBounds.y); 
    }

    public void GameOver()
    {
        //player.SetActive(false);
        isAlive = false;
    }

    public void RestartGame()
    {
        player.SetActive(true);
        isAlive = true;
    }

    public bool IsCharacterAlive()
    {
        return isAlive;
    }

    public void KillPlayer()
    {
        player.SetActive(false);
    }

    public void EnemyKilled(Vector2 EnemyDeathLocation)
    {
        GameObject enEn = Instantiate(enemyEnergy) as GameObject;
        enEn.GetComponent<Collider2D>().enabled = false;
        enEn.transform.position = EnemyDeathLocation;

        Vector3 energySpawn = new Vector3(Random.Range(spawnableArea.x, spawnableArea.y), Random.Range(-screenBounds.y, screenBounds.y));

        StartCoroutine(SpawnEnemyEnergy(enEn, energySpawn));

    }

    IEnumerator SpawnEnemyEnergy(GameObject enemyEnergy, Vector3 energyPos)
    {
        while (enemyEnergy.transform.position != energyPos)
        {
            enemyEnergy.transform.position = Vector2.MoveTowards(new Vector2(enemyEnergy.transform.position.x, enemyEnergy.transform.position.y), energyPos, 6 * Time.deltaTime);
            yield return null;
        }
        enemyEnergy.GetComponent<Collider2D>().enabled = true;

    }

    public void AddSpawnedObject(GameObject spawnedObject)
    {
        spawnedObjects.Add(spawnedObject);
    }

    private void RemoveOffScreenObject()
    {
        foreach (GameObject gameObj in spawnedObjects)
        {
            //if (!render.isVisible) gameObj.SetActive(false);
        }
    }
}
