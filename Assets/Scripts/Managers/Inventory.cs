﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour {

    public static Inventory instance;
    [SerializeField] private GameObject lantern;
    SpawnableObject itemOnTop;
    List<SpawnableObject> items = new List<SpawnableObject>();

    private float backpackWeight;
    private float totalCurrency;
    private float numWhiteFireflies;
    [SerializeField] private float maxWhiteFireflies;

    private int[] fireflyStorage = new int[3];

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            //fDontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        List<SpawnableObject> items = new List<SpawnableObject>();

        resetFireflies();
    }

    void Start () 
    {
        itemOnTop = null;
        numWhiteFireflies = maxWhiteFireflies;
	}


    void Update()
    {
        if (fireflyStorage[0] + fireflyStorage[1] + fireflyStorage[2] == 20)
        {

        }
    }

    private void ImbuePower()
    {

    }

    public void AddToInventory(SpawnableObject gemToAdd)
    {
        items.Add(gemToAdd);
        itemOnTop = items[items.Count - 1];
    }


    float GetBackpackWeight() { return backpackWeight; }

    float GetTotalCurrency() { return totalCurrency; }

    public SpawnableObject GetItemOnTop()
    {
        return itemOnTop;
    }

    public void CollectWhiteFirefly()
    {
        lantern.SetActive(true);
        if (numWhiteFireflies < maxWhiteFireflies) numWhiteFireflies++;
        else 
        { 
            // add bonus firefly points
        }
    }

    public void SubtractWhiteFirefly()
    {
        if (numWhiteFireflies > 0) numWhiteFireflies--;
        if (numWhiteFireflies == 0) lantern.SetActive(false);
    }

    private void resetFireflies()
    {
        fireflyStorage[0] = 0;
        fireflyStorage[1] = 0;
        fireflyStorage[2] = 0;
    }

    public float GetWhiteFireflies() { return numWhiteFireflies; }
}
