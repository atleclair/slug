﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour {

    public static TimeManager Instance;

    public float slowdownFactor = 0.00000005f;
    public float slowdownLength = 2f;
    public bool timeSlowed;

    private float originalDeltaTime;

    // Use this for initialization
    void Start () {

        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        originalDeltaTime = Time.fixedDeltaTime;
	}

    public void Pause()
    {
        Time.timeScale = 0;
    }
	
    public void SlowmoOn()
    {
        Time.timeScale = slowdownFactor;
        Time.fixedDeltaTime = Time.timeScale * .02f;
        timeSlowed = true;
    }

    public void SlowmoOff()
    {
        Time.timeScale = 1;
        Time.fixedDeltaTime = originalDeltaTime;
        GameEvents.instance.PlayerStopAttack();
        timeSlowed = false;
    }

    public void SlowDownWithDuration(float duration)
    {
        StartCoroutine(SlowDown(duration));
    }

    private IEnumerator SlowDown(float duration)
    {
        SlowmoOn();
        yield return new WaitForSeconds(duration);
        SlowmoOff();
    }
}
