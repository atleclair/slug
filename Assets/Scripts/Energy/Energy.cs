﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Energy : ShootableObject
{
    public int index;
    private int rotateSpeed;

    public Throw t;

    public virtual void Update()
    {
        bool isPenalizedCurrently = t.IsPenalized();

        if (isPenalizedCurrently) GetComponent<SpriteRenderer>().color = Color.gray;
        if (!isPenalizedCurrently) GetComponent<SpriteRenderer>().color = Color.white;

        if (EnergyManager.Instance.CountEnergy() == 1 )
        {
            rotateSpeed = 360;
        }

        if (EnergyManager.Instance.CountEnergy() == 2)
        {
            rotateSpeed = 270;
        }

        if (EnergyManager.Instance.CountEnergy() == 3)
        {
            rotateSpeed = 180;
        }

        transform.RotateAround(transform.parent.position, Vector3.forward, rotateSpeed * Time.deltaTime);

        
    }

    public virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (GetComponent<SpriteRenderer>().isVisible)
        {
            if (collision.gameObject.tag.Contains("enemy"))
            {
                if (collision.gameObject.tag.Contains("Projectile"))
                {
                    return;
                }

                else
                {
                    if (!Player.Instance.isShielded)
                    {
                        AudioManager.Instance.Play("Damage");
                        ScreenShake.Instance.TriggerShake(shakeMag: screenShakeAmount);

                        Enemy enemy = collision.gameObject.GetComponent<Enemy>();
                        if (enemy) enemy.TakeDamage(damage);

                        EnergyManager.Instance.UseEnergy();
                    }

                }
            }
        }
    }
}
