﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestingCoordinates : MonoBehaviour
{

    public Vector3 mouse;
    public Vector3 zero;
    public Vector3 screenwidth_height;
    public float screenheight = Screen.height;
    public float screenwidth = Screen.width;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        mouse = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        zero = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0));
        screenwidth_height =  Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0));
    }
}
