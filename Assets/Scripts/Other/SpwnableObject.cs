﻿using UnityEngine;

public abstract class SpawnableObject : MonoBehaviour
{
    [SerializeField]
    protected int spawnPadding = -1;

    // correlates to the spawning grid. this index is used to pull a world
    // coordinate to spawn the object at.
    // This is assigned when the object is spawned by the SpawnManager
    public Vector3 spawnIndex;

    public abstract void Setup();

    public int GetPadding() { return spawnPadding; }
}