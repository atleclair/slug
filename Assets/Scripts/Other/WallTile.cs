﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallTile : MonoBehaviour
{
    bool hasBloomed;
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        int randomRoll = Random.Range(0, 10);

        if (collision.gameObject.tag == "player")
        {
            if (!hasBloomed)
            {
                if (randomRoll <= 2)
                {
                    GetComponent<Animator>().SetTrigger("blue");
                }

                if (randomRoll >2 && randomRoll <= 5)
                {
                    GetComponent<Animator>().SetTrigger("pink");
                }

                if (randomRoll >5 && randomRoll <= 8)
                {
                    GetComponent<Animator>().SetTrigger("white");
                }

                else 
                {
                    GetComponent<Animator>().SetTrigger("all");
                }

            }
        }
    }
}
