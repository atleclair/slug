﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// The orange powerup provides attack enhancements to the player
public class OrangePowerup : Powerup
{
    void Start()
    {
        InitializeEnergyRequirement();
        base.currentPowerup = 1;
        base.numTemp = 0;
        base.numCollected = 0;
        base.numRequired = energyRequired[currentPowerup];
    }

    private void InitializeEnergyRequirement()
    {
        energyRequired.Add(1, 20);
        energyRequired.Add(2, 30);
        energyRequired.Add(3, 40);
        energyRequired.Add(4, 50);
        energyRequired.Add(5, 60);
        energyRequired.Add(6, 70);
        energyRequired.Add(7, 80);
    }

    private void BankFireflies()
    {
        numCollected += numTemp;
        numTemp = 0;
    }

    private void LoseFireflies()
    {
        numCollected /= 2;
    }

    override public void ActivatePowerup(int powerup)
    {
        string message = "";

        switch (powerup) 
        {
            case 0:
                message = "PROJECTILE DISTANCE INCREASED";
                GameEvents.instance.PowerupActivated("orange1");
                break;
            case 1:
                message = "CHARGED PROJECTILE UNLOCKED";
                GameEvents.instance.PowerupActivated("orange2");
                break;
            case 2:
                message = "PROJECTILE REGEN TIME DECREASED";
                GameEvents.instance.PowerupActivated("orange3");
                // TODO: increase speed of projectile
                break;
            case 3:
                message = "BULLET PENETRATION UNLOCKED";
                GameEvents.instance.PowerupActivated("orange4");
                // TODO: unlock special attack: does 2 dmg penetrates through 1 enemy for 2 energy
                break;
            case 4:
                message = "RICOCHET UNLOCKED";
                GameEvents.instance.PowerupActivated("orange5");
                // TODO: projectile ricochets
                break;
            case 5:
                message = "EXPLOSIVE PROJECTILE UNLOCKED";
                GameEvents.instance.PowerupActivated("orange6");
                // TODO: projectile explodes 
                break; 
        }

        PowerupManager.Instance.DisplayPowerupText(message);
    }
}
