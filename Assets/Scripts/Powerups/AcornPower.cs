﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcornPower : MonoBehaviour
{
    public List<string> powerups = new List<string>();
    // Start is called before the first frame update
    void Start()
    {
        powerups.Add("distance");
        powerups.Add("bounce");
        powerups.Add("pierce");
        powerups.Add("explode");
        powerups.Add("shotgun");

        GameEvents.instance.OnEnabledPowerup += Activate;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void Activate()
    {
        if (powerups.Count > 0)
        {
            string powerup = powerups[Random.Range(0, powerups.Count)];
            Activate(powerup);
        }
    }

    void Activate(string powerup)
    {
        switch (powerup)
        {
            case "distance":
            GameEvents.instance.AcornPower_Distance();
            powerups.Remove("distance");
            break;

            case "bounce":
            GameEvents.instance.AcornPower_Bounce();
            powerups.Remove("bounce");
            break;

            case "pierce":
            GameEvents.instance.AcornPower_Pierce();
            powerups.Remove("pierce");
            break;

            case "explode":
            GameEvents.instance.AcornPower_Explode();
            powerups.Remove("explode");
            break;

            case "shotgun":
            GameEvents.instance.AcornPower_Shotgun();
            powerups.Remove("shotgun");
            break;
        }
    }
}
