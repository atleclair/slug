﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// The pink powerup provides utility enhancements to the player
public class PinkPowerup : Powerup
{
    void Start()
    {
        InitializeEnergyRequirement();
        base.currentPowerup = 1;
        base.numTemp = 0;
        base.numCollected = 0;
        base.numRequired = energyRequired[currentPowerup];
    }

    private void InitializeEnergyRequirement()
    {
        energyRequired.Add(1, 20);
        energyRequired.Add(2, 30);
        energyRequired.Add(3, 40);
        energyRequired.Add(4, 50);
        energyRequired.Add(5, 60);
        energyRequired.Add(6, 70);
        energyRequired.Add(7, 80);
    }

    private void BankFireflies()
    {
        numCollected += numTemp;
        numTemp = 0;
    }

    private void LoseFireflies()
    {
        numCollected /= 2;
    }

    override public void ActivatePowerup(int powerup)
    {
        string message = "";

        switch (powerup) 
        {
            case 0:
                message = "ADDITIONAL HEART UNLOCKED";
                GameEvents.instance.PowerupActivated("pink1");
                break;
            case 1:
                message = "LONGER INVULNERABILITY AFTER DAMAGE";
                GameEvents.instance.PowerupActivated("pink2");
                break;
            case 2:
                message = "ADDITIONAL HEART UNLOCKED";
                GameEvents.instance.PowerupActivated("pink3");
                break;
            case 3:
                message = "INCREASED CROUCH SLOW";
                GameEvents.instance.PowerupActivated("pink4");
                break;
            case 4:
                message = "MAGNET UNLOCKED";
                GameEvents.instance.PowerupActivated("pink5");
                break;
            case 5:
                message = "FULL HEAL FROM HEARTS";
                GameEvents.instance.PowerupActivated("pink6");
                break;
        }

        PowerupManager.Instance.DisplayPowerupText(message);
    }
}
