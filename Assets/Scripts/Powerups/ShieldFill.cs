﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShieldFill : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Color temp = GetComponent<SpriteRenderer>().color;
        temp.a = 0.25f;
        GetComponent<SpriteRenderer>().color = temp;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
