﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWallTile: MonoBehaviour
{   
    float damage = .25f;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "player")
        {
            if (!Player.Instance.isShielded) GameEvents.instance.PlayerDamage(damage);
        }
    }
}
