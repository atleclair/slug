﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeafManager : MonoBehaviour
{
    public Slider leafSlider;
    public Image leafFill;

    public int numLeaves;
    public int maxLeaves;

    private float leafRemovalTime;

    public bool isFull;

    private Color flashColor;
    private Color fillColor;

    // Start is called before the first frame update
    void Start()
    {
        GameEvents.instance.OnEnemyDeath += EnemyKilled;

        fillColor = new Color(117f/255f, 255f/255f, 125f/255f);
        flashColor = new Color(1, 1, 1);

        GameEvents.instance.OnFireflyPickup += AddLeaf;

        numLeaves = 0;
        maxLeaves = 15;
        leafRemovalTime = 1;
    }

    // Update is called once per frame
    void Update()
    {
        leafSlider.value = numLeaves;
        leafSlider.maxValue = maxLeaves;

        if (numLeaves == maxLeaves && !isFull)
        {
            isFull = true;
            GameEvents.instance.LeavesFull();
            StartCoroutine(RemoveLeaves());
        }
        
    }

    void EnemyKilled()
    {
        if (isFull) numLeaves++;
    }

    void AddLeaf(CollectibleItem leaf)
    {
        if (!isFull)
        {
            if (numLeaves < maxLeaves)
            {
                StartCoroutine(FlashBar());
                numLeaves++;
            }
        }
    }

    IEnumerator RemoveLeaves()
    {
        while (numLeaves != 0)
        {
            FlashBar();
            yield return new WaitForSeconds(leafRemovalTime);
            numLeaves--;
        }
        isFull = false;
        GameEvents.instance.LeavesEmpty();
    }

    IEnumerator FlashBar()
    {
        leafFill.color = flashColor;
        yield return new WaitForSeconds(.2f);
        leafFill.color = fillColor;
        yield return new WaitForSeconds(.2f);
        leafFill.color = flashColor;
         yield return new WaitForSeconds(.2f);
                 leafFill.color = fillColor;


    }
}
